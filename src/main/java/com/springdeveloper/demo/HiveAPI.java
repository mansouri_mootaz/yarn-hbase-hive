package com.springdeveloper.demo;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

//import org.apache.tools.ant.types.resolver.ApacheCatalog;

import java.sql.DriverManager;

public class HiveAPI {
	/**
	 * <p>
	 * changed from hadoop.hive to apache.hive (new version)
	 * </p>
	 */
	private static String driverName = "org.apache.hive.jdbc.HiveDriver";

	/**
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		System.out.println("Running Hive Program!");

		try {
			Class.forName(driverName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}

		// changed from jdbc:hive to jdbc:hive2(new version)
		Connection con = DriverManager.getConnection("jdbc:hive2://192.168.1.163:10000/mootazdb", "hdfs", "");

		Statement stmt = con.createStatement();
		String tableName = "mootazhivejdbctable";

		// DDL :data definition language for create update .. we use
		// statement.execute
		// DML :data manipulation language for modifications .. we use
		// statement.executeQuery
		Boolean DDL = stmt.execute("create table if not exists " + tableName
				+ " ( received String,Product String,Sub_product String,Issue String,Sub_issue String,Consumer_complaint_narrative String,Company_public_response String,Company  String ,State String,ZIP_code String,Tags String ,Consumer_consent_provided String,Submitted_via String , sent_to_company String,Company_response_to_consumer String,Timely_response String,Consumer_disputed String,ComplaintID String) row format delimited fields terminated by ',' lines terminated by '\n'");

		// show tables
		String sql = "show tables '" + tableName + "'";
		System.out.println("Running: " + sql);
		ResultSet res = stmt.executeQuery(sql);
		while (res.next()) {
			System.out.println(res.getString(1));
		}

		// describe table
		sql = "describe " + tableName;
		System.out.println("Running: " + sql);
		res = stmt.executeQuery(sql);
		while (res.next()) {
			System.out.println(res.getString(1) + "\t" + res.getString(2));
		}

		// load data into table
		// NOTE: filepath has to be local to the hive server
		String filepath = "/user/mootazdata/modified";
		sql = "load data inpath '" + filepath + "' into table " + tableName;
		if (DDL) {
			System.out.println("Running: " + sql);
			DDL = stmt.execute(sql);
		}

		// select * query
		 sql = "select * from " + tableName;
		 System.out.println("Running: " + sql);
		 res = stmt.executeQuery(sql);
		 while (res.next()) {
		 System.out.println(String.valueOf(res.getString(1)) + "\t" +
		 res.getString(8));
		 }

		// regular hive query
		// sql = "select count(*) from " + tableName;
		// System.out.println("Running: " + sql);
		// res = stmt.executeQuery(sql);
		// while (res.next()) {
		// System.out.println(res.getString(1));
		// }

		// complex hive query
//		sql = "select company, Issue from (select company,Issue,count(*) from " + tableName
//				+ " group by company, Issue having count(*) in ( select max(total) as freq from ( select company, Issue ,count(*) as total from "
//				+ tableName + " group by company, Issue) as test_temp group by company)) as final_results";
//		System.out.println("running : " + sql);
//		res = stmt.executeQuery(sql);
//		while (res.next()) {
//			System.out.println(res.getString(1) + "\t" + res.getString(2) + "\t");
//
//		}

		System.out.println("*****Done with Running Hive Program!*****");

	}
}