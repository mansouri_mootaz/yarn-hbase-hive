#Yarn Implementation (Client&ApplicationMaster)
#Author {Mootaz}
==============================================

#mvn clean package
#sudo su hdfs
#hdfs fs -rm /apps/simple/simple-yarn-app-0.1.0-jar-with-dependencies.jar
#hdfs fs -copyFromLocal /path-to-the-project/yarn-examples/simple-yarn-example/target/simple-yarn-app-0.1.0-jar-with-dependencies.jar /apps/simple/
#yarn jar target/simple-yarn-app-0.1.0.jar com.springdeveloper.demo.Client [arg1:number of Containers] [arg2:wich_program{HbaseAPI||HiveAPI}
#go to one of the Node managers (Yarn slaves)
#cd /hadoop/yarn/log/app****/container*****/{stdout,stderror} those are the ouputs of your application
